﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataaccessEFCore.Models
{
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public string CustomerFirstName { get; set; }
        public string GenreName { get; set; }
        public int GenreCount { get; set; }
    }

}
