﻿using DataaccessEFCore.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataaccessEFCore.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        ChinookContext context;
        public CustomerRepository()
        {
            context = new ChinookContext();
        }
        public Customer GetCustomerByName(string name)
        {
            Customer cust = context.Customers.Where(c => c.FirstName.Contains(name)).FirstOrDefault();

            return cust;
        }

        public Customer GetById(int id)
        {
            Customer cust = context.Customers.Find(id);

            return cust;
        }

        public List<Customer> GetAll()
        {
            List<Customer> custList = context.Customers.ToList();

            return custList;
        }

        public bool Add(Customer entity)
        {
            context.Customers.Add(entity);
            bool success = context.SaveChanges() > 0;

            return success;
        }

        public bool Update(Customer entity)
        {
            context.Customers.Update(entity);
            bool success = context.SaveChanges() > 0;

            return success;
        }

        public bool Delete(Customer entity)
        {
            context.Customers.Remove(entity);
            bool success = context.SaveChanges() > 0;

            return success;
        }

        public List<Customer> GetPageOfCustomers(int limit, int offset)
        {
            List<Customer> custList = context.Customers.Skip(offset).Take(limit).ToList();

            return custList;
        }

        public List<CustomerCountry> GetCustomersByCountry()
        {
            List<CustomerCountry> custList = context.Customers.GroupBy(c => c.Country).Select(g => new CustomerCountry { CountryName = g.Key, CustomerCount = g.Count() }).OrderByDescending(g => g.CustomerCount).ToList();

            return custList;
        }

        public List<CustomerSpender> GetTopSpenders()
        {
            List<CustomerSpender> custList = context.Customers
                                                .Select(c => new CustomerSpender 
                                                { 
                                                    CustomerId = c.CustomerId,
                                                    CustomerFirstName = c.FirstName, 
                                                    InvoiceTotal = (double)c.Invoices.Sum(i => i.Total) 
                                                })
                                                .OrderByDescending(cs => cs.InvoiceTotal)
                                                .Take(3)
                                                .ToList();

            return custList;
        }

        public List<CustomerGenre> GetCustomerTopGenres(int id)
        {
            List<CustomerGenre> custList = context.Customers.Where(c => c.CustomerId == id)
                                        .SelectMany(c => c.Invoices, (c, i) =>
                                            new
                                            {
                                                c.CustomerId,
                                                c.FirstName,
                                                i.InvoiceLines
                                            }
                                        )
                                        .SelectMany(o => o.InvoiceLines, (o, il) =>
                                            new
                                            {
                                                o.CustomerId,
                                                o.FirstName,
                                                GenreName = il.Track.Genre.Name // Can access since it's a reference property
                                            }
                                        )
                                        .GroupBy(x => new { cId = x.CustomerId, cFirstName = x.FirstName, gName = x.GenreName })
                                        .Select(group => 
                                            new CustomerGenre 
                                            { 
                                                CustomerId = group.Key.cId, 
                                                CustomerFirstName = group.Key.cFirstName, 
                                                GenreName = group.Key.gName,
                                                GenreCount = group.Count() 
                                            })
                                        .OrderByDescending(cg => cg.GenreCount)
                                        // Couldn't find solution for "SELECT TOP 1 WITH TIES" part :(
                                        .Take(1)
                                        .ToList();

            return custList;
        }
    }
}
