﻿using DataaccessEFCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataaccessEFCore.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Customer GetCustomerByName(string name);
        List<Customer> GetPageOfCustomers(int limit, int offset);
        List<CustomerCountry> GetCustomersByCountry();
        List<CustomerSpender> GetTopSpenders();
        List<CustomerGenre> GetCustomerTopGenres(int id);
    }
}
