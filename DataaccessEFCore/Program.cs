﻿using DataaccessEFCore.Models;
using DataaccessEFCore.Repositories;
using System;
using System.Collections.Generic;

namespace DataaccessEFCore
{
    class Program
    {
        static void Main(string[] args)
        {
            // Remember to change DbContext Connection string

            Console.WriteLine("--- Data Access tests ---");
            // Simulating Dependency Injection (highly simplified)
            ICustomerRepository repository = new CustomerRepository();
            // Passing dependency
            //TestSelectAll(repository);
            //TestSelectByName(repository);
            //TestInsert(repository);
            //TestUpdate(repository);
            //TestDelete(repository);
            //TestSelectById(repository);
            //TestGetPageOfCustomers(repository);
            //TestGetCustomersByCountry(repository);
            //TestGetTopSpenders(repository);
            //TestGetCustomerTopGenres(repository);
        }

        static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAll());
        }

        static void TestSelectByName(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerByName("lu"));
        }

        static void TestSelectById(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetById(1));
        }

        static void TestInsert(ICustomerRepository repository)
        {
            Customer customer = new Customer() { FirstName = "Dewald", LastName = "Els", Email = "Dewald@els.ng", Country = "South Africa", PostalCode = "2830", Phone = "4554545445" };
            repository.Add(customer);
            PrintCustomer(repository.GetCustomerByName("Dewald"));
        }

        static void TestUpdate(ICustomerRepository repository)
        {
            Customer customer = repository.GetCustomerByName("Dewald");
            customer.Country = "Norway";
            repository.Update(customer);
            PrintCustomer(repository.GetCustomerByName("Dewald"));
        }

        static void TestDelete(ICustomerRepository repository)
        {
            Customer customer = repository.GetCustomerByName("Dewald");
            if (repository.Delete(customer))
            {
                Console.WriteLine("Deleted");
            }
            else
            {
                Console.WriteLine("Delete failed");
            }
        }

        static void TestGetPageOfCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetPageOfCustomers(10, 10));
        }

        static void TestGetTopSpenders(ICustomerRepository repository)
        {
            PrintCustomerSpenders(repository.GetTopSpenders());
        }

        static void TestGetCustomerTopGenres(ICustomerRepository repository)
        {
            PrintCustomerGenres(repository.GetCustomerTopGenres(12));
        }


        static void PrintCustomers(List<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- { customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} ---");
        }
        

        static void TestGetCustomersByCountry(ICustomerRepository repository)
        {
            PrintCustomerCountrys(repository.GetCustomersByCountry());
        }

        static void PrintCustomerCountrys(IEnumerable<CustomerCountry> customers)
        {
            foreach (CustomerCountry customer in customers)
            {
                PrintCustomerCountry(customer);
            }
        }

        static void PrintCustomerCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"--- { customerCountry.CountryName} {customerCountry.CustomerCount} ---");
        }

        static void PrintCustomerSpenders(IEnumerable<CustomerSpender> customers)
        {
            foreach (CustomerSpender customer in customers)
            {
                PrintCustomerSpender(customer);
            }
        }

        static void PrintCustomerSpender(CustomerSpender customerCountry)
        {
            Console.WriteLine($"--- { customerCountry.CustomerId} { customerCountry.CustomerFirstName} {customerCountry.InvoiceTotal} ---");
        }

        static void PrintCustomerGenres(IEnumerable<CustomerGenre> customers)
        {
            foreach (CustomerGenre customer in customers)
            {
                PrintCustomerGenre(customer);
            }
        }

        static void PrintCustomerGenre(CustomerGenre customerGenre)
        {
            Console.WriteLine($"--- { customerGenre.CustomerId} { customerGenre.CustomerFirstName} {customerGenre.GenreName} {customerGenre.GenreCount} ---");
        }
    }
}
